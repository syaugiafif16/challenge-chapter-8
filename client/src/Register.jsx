import React, { useState } from "react";
import Table from './Table';

const Register = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [players, setPlayers] = useState([]);
  
    const handleSubmit = () => {
      console.log(username, password, email)
      const listPlayer = {username,password,email}
      if (username && password && email) {
        setPlayers([...players,listPlayer])
        setUsername("")
        setPassword("")
        setEmail("")
      }
    }

    return (
        <div className="App">
          <p className="page">Register Form</p>
          <div className="form">
            <p>Username</p>
            <input type="text" name="username" placeholder="username" value={username} onChange={(e) => setUsername(e.target.value)} />
            <p>Password</p>
            <input type="password" name="password" inp placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            <br></br>
            <p>Email</p>
            <input type="email" name="email" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
            <br></br>
            <button onClick={handleSubmit}>Simpan</button>
            <br></br>
            
          </div>
          
         <Table players={players} onChange={handleSubmit}/> 
    
        </div>
    
        
      );
    
    }
    
    export default Register;
    