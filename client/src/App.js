import './App.css';
import React, { useState, useEffect} from 'react';

import Register from './Register';
import Edit from './Edit';
import Find from './Find';


function App() {
  return (
    <div>
      <Register></Register>
      <Edit></Edit>
      <Find></Find>
    </div>
    
  );

}

export default App;
