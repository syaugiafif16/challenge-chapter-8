import React, { useState } from "react";
import Table from './Table';

const Find = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [players, setPlayers] = useState([]);
  
    const handleSubmit = () => {
      console.log(username, password, email)
      const listPlayer = {username,password,email}
      if (username && password && email) {
        setPlayers([...players,listPlayer])
        setUsername("")
        setPassword("")
        setEmail("")
      }
    }

    return (
        <div className="App">
          <p className="page">Find Player</p>
          <div className="form">
            <p>ID</p>
            <input type="text" name="username" placeholder="username" value={username} onChange={(e) => setUsername(e.target.value)} />
           
            <button onClick={handleSubmit}>Find</button>
            <br></br>
            
          </div>
          
         <Table players={players} onChange={handleSubmit}/> 
    
        </div>
    
        
      );
    
    }
    
    export default Find;
    