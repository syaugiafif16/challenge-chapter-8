import React, { useState } from "react";
import Table from './Table';

const Edit = () => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [players, setPlayers] = useState([]);
  
    const handleSubmit = () => {
      console.log(username, password, email)
      const listPlayer = {username,password,email}
      if (username && password && email) {
        setPlayers([...players,listPlayer])
        setUsername("")
        setPassword("")
        setEmail("")
      }
    }

    return (
        <div className="App">
          <p className="page">Edit Form</p>
          <div className="form">
            <p>New Username</p>
            <input type="text" name="newUsername" placeholder="username" value={username} onChange={(e) => setUsername(e.target.value)} />
            <p>New Password</p>
            <input type="password" name="newPassword" inp placeholder="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
            <br></br>
            <p>New Email</p>
            <input type="email" name="newEmail" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
            <br></br>
            <button onClick={handleSubmit}>Simpan</button>
            <br></br>
            
          </div>
          
         <Table players={players} onChange={handleSubmit}/> 
    
        </div>
    
        
      );
    
    }
    
    export default Edit;
    